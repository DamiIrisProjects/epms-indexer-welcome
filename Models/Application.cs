﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WelcomeNIS.Models
{
    public class Application
    {
        public int AppId { get; set; }

        public string Name { get; set; }

        public bool HasAccess { get; set; }

        public string Icon { get; set; }

        public string Link { get; set; }

        public string Port { get; set; }
    }
}