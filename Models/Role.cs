﻿namespace WelcomeNIS.Models
{
    public class Role
    {
        /// <summary>
        /// Id of role
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of role
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Date role was created
        /// </summary>
        public string DateCreated { get; set; }

        /// <summary>
        /// Id of who created this role
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Namem of who created this role
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// Application linked to role
        /// </summary>
        public int ApplicationId { get; set; }
        
        /// <summary>
        /// Role linked to role
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// User linked to role
        /// </summary>
        public int UserId { get; set; }
    }
}