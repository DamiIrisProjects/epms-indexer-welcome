﻿using AspNet.Identity.Oracle;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WelcomeNIS.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Name")]
        [StringLength(100)]
        public string FullName
        {
            get
            {
                return FirstName + " " + Surname;
            }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : OracleDatabase
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public ApplicationDbContext(string connectionStringName)
            : base(connectionStringName)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public static class GenericPrincipalExtensions
    {
        public static string FullName(this IPrincipal user)
        {
            return Helpers.Helper.ToFirstLetterUpper(GetClaim(user, "Fullname"));
        }

        public static string GetClaim(IPrincipal user, string claimsearch)
        {
            string result = "";

            if (user.Identity.IsAuthenticated)
            {
                var claimsIdentity = user.Identity as ClaimsIdentity;
                if (claimsIdentity != null)
                    foreach (var claim in claimsIdentity.Claims)
                    {
                        if (claim.Type == claimsearch)
                            result = claim.Value;
                    }
            }

            return result;
        }
    }

    public class AuthorizeCustomAttribute : AuthorizeAttribute
    {
        // Custom property
        public string AccessLevel { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
                return false;
            }

            // set operator type
            string result = GenericPrincipalExtensions.GetClaim(httpContext.User, "OperatorType");

            // if you cannot find the claim, sign him in again
            if (string.IsNullOrEmpty(result))
            {
                HttpContext.Current.GetOwinContext().Authentication.SignOut();
                return false;
            }
            else
                return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 403;
            }
            else
            {
                filterContext.Result = new RedirectResult("~/Home/Index");
            }
        }
    }
}