﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UMAentities;
using WelcomeNIS.Helpers;
using WelcomeNIS.Models;
using Helper = WelcomeNIS.Helpers.Helper;

namespace WelcomeNIS.Controllers
{
    public class HomeController : Controller
    {
        #region Config values & defaults

        private readonly string _baseuri = ConfigurationManager.AppSettings["serviceurl"];
        private readonly string _baselink = ConfigurationManager.AppSettings["baselink"] ?? "https://localhost";
        private readonly string _appentry = ConfigurationManager.AppSettings["appentry"] ?? "Login"; 

        #endregion

        public async Task<ActionResult> Index(string requestsent)
        {
            // If just requested access, show msg
            if (!string.IsNullOrEmpty(requestsent) && requestsent == "1")
                ViewBag.RequestSent = 1;

            if (User.Identity.IsAuthenticated)
            {
                // Just a quick check
                string userid = GenericPrincipalExtensions.GetClaim(User, "Id");
                if (string.IsNullOrEmpty(userid) || string.IsNullOrEmpty(User.FullName()))
                {
                    HttpContext.GetOwinContext().Authentication.SignOut();
                    return RedirectToAction("Login", "Account");
                }

                // Get all applications from database
                var applications = await new ApiClientProvider().GetApplicationsAsync();

                // Get all user roles
                var roles = JsonConvert.DeserializeObject<List<Models.Role>>(GenericPrincipalExtensions.GetClaim(User, "Roles"));
                

                // Setup applications and visibility
                var apps = applications as IList<Application> ?? applications.ToList();
                foreach (var role in roles)
                {
                    var app = apps.FirstOrDefault(x => x.Id == role.ApplicationId);

                    if (app != null)
                        app.HasAccess = true;
                }

                // Reorder based on what the person can see, then by alphabet
                ViewBag.Applications = apps.OrderBy(x => x.Name).ThenByDescending(x => x.HasAccess).ToList();

                return View("Applications");
            }
            else
            {
                Response.StatusCode = 403;
                return View();
            }
        }

        #region Operations

        [Authorize403]
        public async Task<ActionResult> CreateLoginToken(string appid)
        {
            try
            {
                if (!string.IsNullOrEmpty(appid))
                {
                    string token = await CreateLoginTokenAsync();

                    // Get application port
                    var applications = await new ApiClientProvider().GetApplicationsAsync();
                    var app = applications.FirstOrDefault(x => x.Id.ToString() == appid);

                    // Depending on the application, create a link
                    if (app != null)
                    {
                        string link = $"{_baselink}:{app.Port}/{_appentry}?id={token}";
                        return Json(new { result = 1, link }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = -1 }, JsonRequestBehavior.AllowGet);
        }


        private async Task<string> CreateLoginTokenAsync()
        {
            HttpResponseMessage response;
            Uri baseRequestUri = new Uri(_baseuri + "/api/operator/ExtToken");

            using (var client = new HttpClient())
            {
                Helper.AccessToken = GenericPrincipalExtensions.GetClaim(User, "Token");
                Helper.SetClientAuthentication(client);
                ServicePointManager.ServerCertificateValidationCallback +=
                    (sender, cert, chain, sslPolicyErrors) => true;

                response = await client.GetAsync(
                    new Uri(baseRequestUri, string.Empty));
            }


            var jsonString = await response.Content.ReadAsStringAsync();
            dynamic data = Newtonsoft.Json.Linq.JObject.Parse(jsonString);

            return data.token;
        }       

        #endregion
    }
}