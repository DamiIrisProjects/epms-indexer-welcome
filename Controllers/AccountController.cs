﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WelcomeNIS.Helpers;
using WelcomeNIS.Models;

namespace WelcomeNIS.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ApplicationRoleManager _roleManager = null;
        private ApplicationUserManager _userManager;
        private SignInHelper _helper;

        public ApplicationUserManager UserManager => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
        
        public ApplicationRoleManager RoleManager => _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();

        private SignInHelper SignInHelper => _helper ?? (_helper = new SignInHelper(UserManager, AuthenticationManager, RoleManager));

        [AllowAnonymous]
        public ActionResult Login(string plslogin)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            if (plslogin == "1")
                ViewBag.JustRegistered = true;
            if (plslogin == "2")
                ViewBag.JustUpdated = true;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Create an http client provider:
            var provider = new ApiClientProvider(Helper.HostUriString);

            // Get Token
            var tokenDictionary = await provider.GetTokenDictionary(model.Email, model.Password);

            if (tokenDictionary?["access_token"] != null)
            {
                // Clear any partial cookies from external or two factor partial sign ins
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, tokenDictionary["UserId"])
                };

                var userIdentity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

                userIdentity.AddClaim(new Claim("Id", tokenDictionary["UserId"], ClaimTypes.NameIdentifier));
                userIdentity.AddClaim(new Claim("Fullname", tokenDictionary["Name"]));
                userIdentity.AddClaim(new Claim("Firstname", tokenDictionary["Firstname"]));
                userIdentity.AddClaim(new Claim("Token", tokenDictionary["access_token"]));
                userIdentity.AddClaim(new Claim("Surname", tokenDictionary["Surname"]));
                userIdentity.AddClaim(new Claim("Email", tokenDictionary["Email"]));
                userIdentity.AddClaim(new Claim("OperatorType", tokenDictionary["RoleTypeId"]));
                userIdentity.AddClaim(new Claim("OperatorTypeName", tokenDictionary["RoleTypeName"]));
                userIdentity.AddClaim(new Claim("Branch", tokenDictionary["Branch"]));
                userIdentity.AddClaim(new Claim("Roles", tokenDictionary["Roles"]));

                AuthenticationProperties authprop = new AuthenticationProperties { IsPersistent = false };
                AuthenticationManager.SignIn(authprop, userIdentity);

                return RedirectToLocal(returnUrl);
            }

            ModelState.AddModelError("ServerResponse", "Invalid username or password");
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register(string setprofile)
        {
            RegisterViewModel model = new RegisterViewModel();

            if (!string.IsNullOrEmpty(setprofile))
            {
                if (User.Identity.IsAuthenticated)
                {
                    model.Email = GenericPrincipalExtensions.GetClaim(User, "Email").ToLower();
                    model.ProfileUpdate = true;
                    model.OperatorId = int.Parse(User.Identity.GetUserId());
                }
                else
                    return RedirectToAction("Login");
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.Firstname, Surname = model.Surname, PhoneNumber = model.PhoneNumber, LockoutEnabled = true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home", new { requestsent = 1 });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        
        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code }, Request.Url?.Scheme);

                try
                {
                    // Create mail
                    string buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:180px; height:30px; color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; font-size: 15px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; -webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";
                    string button = "<a style='text-decoration: none' href=\"" + callbackUrl + "\"><div Style='" + buttonstyle + "'>Reset My Password</div></a>";

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<div style='color:#333'>Hello, </div><br/>");
                    sb.Append("<div style='color:#333'>Following your request on the U.M.A. portal, here is a link from which you can change your password:</div><br/>");
                    sb.Append(button);
                    sb.Append("<div style='color:#333; margin: 0px 0px 15px 0px'><b>Note:</b> You must be on the Iris network for this to work.</div>");
                    sb.Append("<div style='color:#333; margin: 0px 0px 10px 0px'>Sincerly,</div><br/>");
                    sb.Append("<div style='color:#333'>The UMA Team</div>");

                    await UserManager.SendEmailAsync(user.Id, "Reset Password", sb.ToString());
                    return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code, string userId)
        {
            var user = UserManager.FindById(userId);

            if (user == null)
                return RedirectToAction("Index", "Home");

            var model = new ResetPasswordViewModel
            {
                Email = user.Email
            };
            return code == null ? View("Error") : View(model);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Error");
            }

            var user = await UserManager.FindByNameAsync(model.Email);

            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInHelper.ExternalSignIn(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/LogOff
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("ServerResponse", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}