﻿using System.Web.Optimization;

namespace WelcomeNIS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/hoverintent.js",
                        "~/Scripts/jquery.lightbox_me.js",
                        "~/Scripts/master.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/lightbox").Include(
                       "~/Scripts/jquery.lightbox_me.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      //"~/Content/page.css",
                      "~/Content/misc.css",
                      "~/Content/inputs.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/select2/select2.css"));

            bundles.Add(new StyleBundle("~/Content/master").Include(
                      "~/Content/misc.css", 
                      "~/Content/masterpage.css"));

            bundles.Add(new StyleBundle("~/Content/home").Include(
                    "~/Content/home.css"));

            bundles.Add(new StyleBundle("~/Content/login").Include(
                  "~/Content/login.css"));

            bundles.Add(new StyleBundle("~/Content/applications").Include(
                  "~/Content/applications.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
