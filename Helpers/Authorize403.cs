﻿using System.Web.Mvc;

namespace WelcomeNIS.Helpers
{
    public class Authorize403 : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpStatusCodeResult(403);
        }
    }
}