﻿// Add for Identity/Token Deserialization:
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
// Add Usings:
using System.Net.Http;
using System.Threading.Tasks;
using UMAentities;

namespace WelcomeNIS.Helpers
{
    public class ApiClientProvider
    {
        #region Variables

        private readonly string _hostUri;

        public string AccessToken { get; }

        #endregion

        #region Constructor

        public ApiClientProvider()
        {
        }

        public ApiClientProvider(string accessToken)
        {
            AccessToken = accessToken;
        }

        public ApiClientProvider(string hostUri, string accessToken)
        {
            _hostUri = hostUri;
            AccessToken = accessToken;
        }

        #endregion

        #region Login

        public async Task<Dictionary<string, string>> GetTokenDictionary(
           string userName, string password)
        {
            HttpResponseMessage response;
            var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>( "grant_type", "password" ), 
                    new KeyValuePair<string, string>( "username", userName ), 
                    new KeyValuePair<string, string> ( "password", password )
                };
            var content = new FormUrlEncodedContent(pairs);

            using (var client = new HttpClient())
            {
                // Allow untrusted certificates since we are using self-certificates on the server
                ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;
                var tokenEndpoint = new Uri(new Uri(_hostUri), "Token");
                response = await client.PostAsync(tokenEndpoint, content);
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return GetTokenDictionary(responseContent);
        }


        private Dictionary<string, string> GetTokenDictionary(
            string responseContent)
        {
            Dictionary<string, string> tokenDictionary =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(
                responseContent);
            return tokenDictionary;
        } 

        #endregion

        #region Applications

        public async Task<IEnumerable<Application>> GetApplicationsAsync()
        {
            HttpResponseMessage response;
            Uri baseRequestUri = new Uri(Helper.HostUriString + "/api/application");

            using (var client = new HttpClient())
            {
                // Allow untrusted certificates since we are using self-certificates on the server
                ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;

                Helper.SetClientAuthentication(client);
                response = await client.GetAsync(baseRequestUri);
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"API Error: Status Code: {response.StatusCode}");
            }

            var jsonString = await response.Content.ReadAsStringAsync();
            var applicationslist = JsonConvert.DeserializeObject<IDictionary<string, List<Application>>>(jsonString);
            return applicationslist.FirstOrDefault().Value;
        }

        #endregion
    }
}
