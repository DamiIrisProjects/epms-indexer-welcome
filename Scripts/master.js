﻿$(function () {
    // Login stuff on the top right
    $('.accountoptions').click(function () {
        $("#menu2").fadeIn();
    });

    // Minor adjustment to set width of login dropdown depending on name width
    $('.outersub').css("min-width", ($('#divLoggedIn').width()) + "px").css("left", "-" + ($('#divLoggedIn').width() - 28) + "px");

    $("#menu li.menuitem").hoverIntent(function () {
        if (!$(this).is(':animated'))
            $(this).find("div.outersub").hide().fadeIn();
    },
     function () {
         $(this).find("div.outersub").fadeOut(100);
     });

    $("#divLoggedIn").hoverIntent(function () {
        if (!$(this).is(':animated'))
            $(this).find("div.outersub").hide().slideDown();
    },
    function () {
        if (!$(this).is(':animated'))
            $(this).find("div.outersub").slideUp(100);
    });


    $("#menu li.menuitem div.submenu div.ssmenu").hoverIntent(function () {
        if (!$(this).is(':animated'))
            $(this).find("div.submenu").hide().fadeIn();
    },
    function () {
        if (!$(this).is(':animated'))
            $(this).find("div.submenu").fadeOut(100);
    });

    // Pop ups
    $(document).on('click', '.closeme, .closebtn, .closeButton', function () {
        $('.PopupWindow').fadeOut(function () { $(this).trigger('close') });
    });

    // Support div
    $('.vertical').click(function () {
        $(".slideout").animate({ width: 'toggle' }, 1000);
    });
    
    // Logged out errors
    $(document).ajaxError(function (event, jqxhr, settings, exception) {
        if (jqxhr.status == 403) {
            window.location.href = '/';
        }
    });
});

function GoToApp(id)
{
    $('#appsdiv').addClass('loading');
    $.ajax({
        type: "GET",
        url: "/Home/CreateLoginToken",
        data: { appid: id },
        dataType: "json",
        error: function (result) {
            $('#appsdiv').removeClass('loading');
            $('#popupmsgInner').html('Unable to connect to server');
            $('#PopupMsg').lightbox_me({
                centered: true
            });
        },
        success: function (result) {
            $('#appsdiv').removeClass('loading');
            if (result.result == 1)
                $("<a>").attr("href", result.link).attr("target", "_blank")[0].click();
            else
            {
                $('#popupmsgInner').html('Error opening application. Please contact administrator via the Help Desk.');
                $('#PopupMsg').lightbox_me({
                    centered: true
                });
            }
        }
    });
}